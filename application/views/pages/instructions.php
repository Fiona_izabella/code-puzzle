<div class="menu-header">
	<h1>instructions</h1>
</div>
<div class="snippets">

	<div id="instructions" title="Instructions">

		<dl class="accordion">

			<dt><a href="">New User</a></dt>
			<dd>After registering, you log in as a new user and need to go through the initial process of 
			carrying out the questions. You need to follow the process <span class ="bold">step one -> step 
			three</span> which leads to the loading of your questions. You then go through each question. 
			After each question you will be shown a review of your question and asked to 
			select <span class ="bold">continue</span>. Once all questions are finished, 
			you will be asked to exit and you will be ready to start your repetitions.
			</dd>

			<dt><a href="">Doing rehearsals</a></dt>
			<dd>Log in to the application, <span class ="bold">select play->load your questions</span> and you will be in the <span class ="bold">question menu</span>.
			You will find each question with its review date. Select the question that is ready for rehearsal.
			You may only view a question when it is ready for rehearsal.</dd>

			<dt><a href="">Notifications</a></dt>
			<dd>You can check if your question is ready for review in the <span class ="bold">question menu</span>.
			Code puzzle will also send you an email notfication when your question is ready for viewing. You have 5 days within which to complete the rehearsal.</dd>

				<dt><a href="">Missing a rehearsal</a></dt>
				<dd>If you do not complete your question within 5 days, you will need to reset the question. 
					It will then be set to that day, allowing you a range of 5 days for completeion.
					However the algorithm will be set back to default, and be treated as a new question.</dd>

					<dt><a href="">Customizing settings</a></dt>
					<dd>On the <span class ="bold">settings page</span> you can change your application skin, turn audio settings on and off, and choose not to
					have email notifications.</dd>

					<dt><a href="">Getting the most out of Code puzzle</a></dt>
					<dd>Code puzzle creates an algorithm which sets a review date for a question depending on your performance on a question.
					If you carry out the rehearsals at the intended time, your learning will be much more effective and reap the benefits of spaced repetition and
					research proven to optimize learning. For more information on spaced repetition and the SM-2 algorithm
					used in this application refer to our <span class ="bold">Online references</span> page.</dd>

					<dt><a href="">Trouble shooting</a></dt>
					<dd>For quick queries please go to our <span class ="bold">Help Search</span> drop down page or our <span class ="bold">F and Q page</span>. If you do not find
					your answer please do not hesitate to send us an online query through our Contact page. Your questions
					will be promptly answered and your feedback and opinions are valuable to our research.</dd>

				</dl>

			</div>

			<div class="menu-links-alt raise-margin">
				<ul>
					<?php echo anchor('fandq','<li>F and Q</li>', array('title' => 'f and Q'));?>
					<?php echo anchor('references','<li>Online references</li>', array('title' => 'Online References'));?>
					<?php echo anchor('contact','<li>contact us</li>', array('title' => 'Contact us'));?>
				</ul>
			</div>
		</div>


