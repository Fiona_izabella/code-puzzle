<div class="menu-header">
	<h1>Welcome to code puzzle</h1>
</div>
<div class="snippets">
	<div class="menu-links">
		<p class="expanded"><span class="bold">Step one: </span>Create your question database</p>

		<?php echo form_open("welcome/add_table"); ?>
		<!-- this leads to a function which creates a new table of questions for the user -->
		<p>
			<input type="submit" class="btn_orange-alt lineHght" value="Create table" />
		</p>
		<?php echo form_close(); ?>

	</div>

</div>
