<div class="menu-header">
<h1>Progress</h1>
</div>
<?php
$qcount = count($questionCount);
?>

<div class="menu-links">
<ul>
    <?php echo anchor('questionmenu','<li class="box-menu">Load questions</li>', array('title' => 'Load questions'));?>   
</ul>
</div>

<div class="snippets-drag">
<h1 class="bold center">Your Progress</h1>
<?php


$today = date("d-m-Y");
$earliestReviewDate =  date('d-m-Y', strtotime($getUsersNextReview->nextReview));
$earliestReviewDatePlus5 = date('d-m-Y', strtotime($earliestReviewDate . ' + 5 days'));


if (isset($earliestReviewDate)): ?>

   
           <?php if( strtotime($getUsersNextReview->nextReview) > time()) :?>
            <p class="center expanded"><i class="fa fa-clock-o fa-fw"></i>Your next review date is:</p>
            <p class="center expanded"><?php  echo $earliestReviewDate; ?></p>

           <?php endif; ?>

<!-- If user has missed their earliest review date -->
           <?php if( strtotime($getUsersNextReview->nextReview) < time()) :?>

                  <!-- check to see whether the question is in range -->
                          <?php  if (check_in_range($earliestReviewDate, $earliestReviewDatePlus5, $today)) :?>
                          <p class="center expanded"><i class="fa fa-exclamation fa-fw"></i>Your rehearsal was due on</p>
                          <p class="center expanded"><?php  echo $earliestReviewDate; ?></p>
                          <p class="center expanded">and is ready to be reviewed.</p>
                          <?php else:?>
                         <p class="center expanded"><i class="fa fa-warning fa-fw"></i>You have missed your earliest review date</p>
                         <p class="center expanded">Due date: <?php  echo $earliestReviewDate; ?></p>
                         <p class="center expanded">Reset the question in the question menu.</p>
                          <?php endif; ?>
    

            <?php endif; ?> 



<?php endif;?>


<hr>

<?php
$count = 1;
$questionReview1 = $getUserNextReviewq1->nextReview;
$questionReview2 = $getUserNextReviewq2->nextReview;
$questionReview3 = $getUserNextReviewq3->nextReview;
$questionReview4 = $getUserNextReviewq4->nextReview;
$questionReview5 = $getUserNextReviewq5->nextReview;
$questionReview6 = $getUserNextReviewq6->nextReview;
$questionReview7 = $getUserNextReviewq7->nextReview;
$questionReview8 = $getUserNextReviewq8->nextReview;
$questionReview9 = $getUserNextReviewq9->nextReview;
$questionReview10 = $getUserNextReviewq10->nextReview;

while ($count <= $qcount)
  {

  // check to see if the question exists. This is done by calling a helper function

  if (checkQuestionExists($count)):
    echo '<h2>Question ' . $count . ' results</h2>';
    echo '<p class="indent">Next Review for Question ' . $count . ': ';

    $test = $
      {
      "questionReview" . $count
      };
    $date = new DateTime($test);
    echo $date->format('d-m-Y');
    echo '<div class="bar_results">';
    foreach($all_q_values as $row): ?>
             <?php
      if ($row->question_id == $count): ?>
                  <div class="sq<?php
        echo $row->q; ?>"><?php
        echo $row->q; ?></div>
                  <?php
      endif; ?>
             <?php
    endforeach;
    echo '</div>';
    echo '<button type="button" class="box-menu-showTable showTable" value="Show results table">Open results table</button>';
    echo '<div class ="results_table">';
    echo '  <table>';
    foreach($all_q_values as $row): ?>
                  <?php
      if ($row->question_id == $count): ?>
                    <tr>             
                     <td>Q = <?php
        echo $row->q; ?> &nbsp;&nbsp;</td>                  
                     <td>Date = <?php
        echo $row->nextReview; ?>&nbsp;&nbsp;</td>
                     </tr>               
                  <?php
      endif; ?>
             <?php
    endforeach;
    echo '</table>';
    echo '<a href="#"><img src="assets/images/button_cross.png" class = "close_table" alt="close table" title="close table"></a>';
    echo '</div><hr>';
  endif;
  $count++;
  } ?>
          <!--end of while loop-->

       
      

</div>





