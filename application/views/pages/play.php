<div class="menu-header">
	<h1>Play</h1>
</div>
<div class="snippets">
	<div class="menu-links">
		<ul>

			<!-- Direct user depending on which state they are in their session, either 'new_user' or 'playQuestion' -->
			<!-- If new user -->
			<?php if ($this->session->userdata('state') == "new_user"):?>

				<p class="expanded"><span class="bold">Step three: </span>Load your questions</p>
				<p><i class="fa fa-info-circle fa-fw"></i>You will be presented with 10 questions. 
					After completing each question you will be reviewed and asked to coninue to the next 
					question. Please finish all 10 questions.</p>
					<p class="expanded"><i class="fa fa-warning fa-fw"></i><span class="bold">The timer activates when you press the button..  </span><span class="bold white">lets go...</span></p>
					<a href="puzzle" class="loadPage"><li class="btn_orange">load your questions</li></a>
						
					<!-- If returning user -->
				<?php elseif ($this->session->userdata('state') == "playQuestion"):?>
					<a href="questionmenu" class="loadPage"><li class="box-menu play-ready">load your questions</li></a>
					<?php echo anchor('progress','<li class="box-menu">your progress</li>', array('title' => 'Your progress'));?>
				<?php endif; ?>
				
			</ul>
		</div>

	</div>
