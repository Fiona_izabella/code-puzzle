<?php
if (!$this->session->userdata('logged_in') == TRUE)
{
    redirect('login');
}

$session_id = $this->session->userdata('id');
$totalNumbReviews = count($questionReviewDate);
$reviewDates = [];
$enable_button = true;

for ($i = 0; $i < $totalNumbReviews; $i++)
{
    $newDate = $questionReviewDate[$i]['nextReview'];
    array_push($reviewDates, "$newDate");
}

?>


<!-- ================  MAIN HTML =====================-->
<div class="menu-header">
    <h1>questions menu</h1>
</div>
<div class="snippets-drag">

    <?php
    $today = date("d-m-Y"); ?>
    <?php
    echo "<h3 class='question-menu-white'>Today is " . $today . "</h3>"; ?>
    <button id="disableQuestions">Question disabled : off</button>
    <div class="clearfix"></div>


    <div class="questionSection">

        <!--create a while loop-->

        <?php
        $count = 1;
        $qcount = count($questionCount);

        while ($count <= $qcount)
        {

            echo '<form name="questionmenu1" class="questionmenuForm" action="' . base_url() . 'puzzle" method="post">';
            echo '<input type="hidden" name="question_number" id="id" value= "' . $count . '" />';
            echo '<h3>Question' . $count . '</h3>';
            echo '<div class="timeReviewInfo">';
            $i = $count - 1;
            if (checkQuestionExists($count)):
                $newReviewDate = $reviewDates[$i];
          
            $date = new DateTime($newReviewDate);
            $newDate = $date->format('d-m-Y');
            echo '<h2>Review date <span class="lighten">' . $newDate . '</span></h2>';
            $newReviewDatePlus5 = date('d-m-Y', strtotime($newReviewDate . ' + 5 days'));
            echo '<h2> Last day to review <span class="lighten">' . $newReviewDatePlus5 . '</span></h2>';

            $date1 = new DateTime($newDate);
            $date2 = new DateTime($today);
            $interval = $date1->diff($date2);

            $date3 = new DateTime($newReviewDatePlus5);
            $date4 = new DateTime($today);
            $intervalPassed = $date3->diff($date4);


            if (check_in_range($newDate, $newReviewDatePlus5, $today))
            {

                echo '<input type="submit" class="readyButt" value= "Load question ' . $count . '" />';
            }
             //if last day to review is greater than todays date
            else if(strtotime($newReviewDatePlus5) > strtotime($today)){

               echo "<p class ='wrong indent'> Question not ready to view </p>";
               echo "<h2>Days till review time: <span class='lighten'>" . $interval->days . " days</span></h2>";
               echo '<input type="submit" class="notreadyButt" value= "question ' . $count . ' not ready to view " />';
           }
           else  if (strtotime($newReviewDatePlus5) < strtotime($today))
           {
               
            echo "<p class ='wrong indent'> You have missed your review date. </p>";
            echo "<h2>You have missed your review by  " . $intervalPassed->days . " days </h2>";
            echo '<p class ="indent">Please reset the question to continue learning</p>';

            
            //Javascript function is called which resets a missed rehearsal
            echo '<a href="#"><li class = "altButton">RESET Question '. $count . '</li></a>';

        }

        endif;
        echo '</form>';
        echo '</div>';
        $count++;
    } //end of while loop

    ?>


</div><!--end of question-->


</div><!--end of page wrapper-->
