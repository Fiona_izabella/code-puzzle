<div class="login-page">
	<div class="login-box-wrapper">

		<div class="logo">
			<img src="assets/images/logo.png">
		</div>

		<div class="box">
			<h2>Log in to Code Puzzle</h2>
		</div>

		<div class="login-box">
			<!--print out flashdata error message if login fails-->
			<p class = "emailflashdata">
				<?php if ($this->session->flashdata('error') != ''): 
				echo $this->session->flashdata('error'); 
				endif; ?>
			</p>

			<?php 
			$attributes = array('class' => 'log-in-form', 'id' => 'log-inForm');
			echo form_open('', $attributes);?>

			<?php echo form_input(array('name' => 'email','placeholder' => 'Email')); ?><?php echo form_error('email', '<div class="logErrors">', '</div>'); ?>
			<?php echo form_input(array('name' => 'password','placeholder' => 'Password', 'type' => 'password')); ?><?php echo form_error('password', '<div class="logErrors">', '</div>'); ?>
			<button class="btn-login-submit" value="Register"><a href="registration" id="loginButton"  alt="Register" title="Register">Register</a></button> 

			<?php echo form_submit('submit', 'Log in', 'class="btn-login-submit-green"'); ?>

			<?php echo form_close();?>
		</div>


	</div>
</div>

