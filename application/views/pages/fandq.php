<div class="menu-header">
	<h1>F and Q</h1>
</div>
<div class="snippets">
	

	<div id="instructions" title="FandQ">


		<dl class="accordion">

			<dt><a href="">Why can I only review at a certain time?</a></dt>
			<dd>Codepuzzle finds the optimum time for your rehearsal from studies and research on long-term memory. If you carry out
			a reherasal before the intended time it is detrimental to the memoty process. Similarly if you carry out a rehearsal too many
			days after your rehearsal date it will have passed the optimal point for memory storage and will be needed to be treated
			as a new question which has not been viewed before. For more information about the reasons please 
			refer to our <span class ="bold">Online references</span></dd>

			<dt><a href="">What happens if I miss a rehearsal?</a></dt>
			<dd>If you do not complete your question within 5 days, you will need to reset the question. 
			It will then be set to that day, allowing you a range of 5 days for completeion.
			However the algorithm will be set back to default, and be treated as a new question..</dd>

			<dt><a href="">How does the application help me remember?</a></dt>
			<dd>Code puzzle creates an algorithm from your performance depending on how long it takes 
			you to complete a puzzle and the amount of attempts you had (out of three). The algorithm 
			is based on the famous SM-2 algorithm created by Piotr Wozniak for 'Super memo' and adjusted
			for the applications puzzle functionality. Research and experiments helped find the best 
			suited algorithm to work alongside code programming puzzles. The algorithm works on the idea 
			that it finds the optimum time for rehearsal, which is the point that you about about to 
			forget the information. In theory your rehearsal intervals should increase with time as your 
			retention levels and memory of the information gets stronger.</dd>

			<dt><a href="">How does the algorithm work?</a></dt>
			<dd>The history of the SM-2 algorithm and its corresponding research is found on http://www.supermemo.com.
			To summarize, the algorithm uses the forgetting curve to calculate the point of which you about to forget a piece of information
			according to your performance on the current repetition. Most spaced repetiton programmes ask the user to rate their performance and use the
			rated score in the algorithm to calculate the next rehearsal. Code puzzle does things differently. We calculate the score/rating
			from your performance on each puzzle, thereby getting an accurate score on how well you remember the puzzle.</dd>

			<dt><a href="">How do I contact the administrator?</a></dt>
			<dd>You can send Code puzzle a query by emailing your query on the <span class ="bold">Contact us</span> page.</dd>

			<dt><a href="">What is the best thing to do to aid learning programming</a></dt>
			<dd>Code puzzle will help you memorize code programming syntax. However it is important to understand the code. Code puzzle is designed to be a 
			suppliment to other learnign applications which aid in learning how to programme. Code puzzle is aimed at helping you to memorize well-structured code syntax.</dd>

			<dt><a href="">How long does it take to store information into long term memory?</a></dt>
			<dd>This depends on each individual. Every person has there own individualised learning pattern 
			and time scale. This is why the algorithm adapts to your own learning and each question is handled 
			separately depending on how difficult you find it. The SM algorithm is designed to re-plot your 
			individual forgetting curve according to your own pace and your own learning.</dd>

		</dl>


	</div>

	<div class="menu-links-alt">

		<ul>
			
			<?php echo anchor('instructions','<li>Instructions</li>', array('title' => 'instructions'));?>
			<?php echo anchor('references','<li>Online references</li>', array('title' => 'references'));?>
			<?php echo anchor('contact','<li>contact us</li>', array('title' => 'Contact us'));?>


		</ul>


	</div>

</div>

