<?php
if (! $this->session->userdata('logged_in')==TRUE)
{
redirect('login');
echo "you are not logged in";
}

/*lOGIC and declaring variables*/
$qcount = count($questionCount);
$actualQuestion = $getQuestion->question;
$questionNumber = 3;
$session_id = $this->session->userdata('id');
$n = $getQuestionInfo[0]['n'];
$oEF = $getQuestionInfo[0]['EF'];
$LstInt  = $getQuestionInfo[0]['newInt'];
$array2 = array();
$datasetCount = count($codePiecesArray[0]); 
$snippetsFromDatabase = array();
$idsFromDatabase = array();
$answerFromDatabase = array();
$answerToPrint = array();
$ansToPrint = array();
$a = array();


//need to place the results arrays into new arrays

$i = 0; 
foreach ($codePiecesArray[0] as $piece) {
  $i++; 
  array_push($snippetsFromDatabase,"$piece");
} 

$i = 0; 
foreach ($codePiecesIds[0] as $pieceId) {
  $i++; 
  array_push($idsFromDatabase,"$pieceId");
} 

$i = 0; 
foreach ($getAnsPrint[0] as $printAns) {
  $i++; 
  array_push($answerToPrint,"$printAns");
} 

$i = 0; 
foreach ($codeTextAnswers[0] as $c) {
  $i++; 
  array_push($ansToPrint,"$c");
} 

$count = 0;
while(count($idsFromDatabase) > $count){
  $count++;
}
?>



<!-- ================  MAIN HTML =====================-->


<div class="testingInfo">
  <div class ="question">

    <?php

    foreach($codeTextAnswers[0] as $printTextAns)
    {
      array_push($a, htmlspecialchars($printTextAns));
      $js_array = json_encode($a);
    }

    ?>

  </div>

  <p><span id="timeField"></span> Drag 3 snippets in the correct order to create the answer</p>
</div>

<!-- Pause display -->
<div class = "pauseSheet">
  <h1 class="center">Puzzle paused</h1>
  <p class="expanded">click on play button ro resume</p>
  <img src="assets/images/hand.png">
</div>

<!-- Display the code snippets from the database -->
<div class="snippet-question">

  <p><?php
    echo $actualQuestion; ?></p>
  </div>

  <div class="snippets-drag">

    <div id="draggable">
      <img src="assets/images/arrow.png">

      <?php
      foreach($codePiecesIds as $divId): ?>
      <?php
      for ($i = 0; $i < count($snippetsFromDatabase); $i++): ?>

      <div id= "<?php
      echo $idsFromDatabase[$i]; ?>"class="square"><?php
      echo htmlspecialchars($snippetsFromDatabase[$i]) ?></div>    
      <?php
      endfor; ?>
      <?php
      endforeach; ?>

    </div>
  </div><!--end of snippets-->


<!-- Display the place holders -->
  <div class="snippets-drop">

    <p class="drophere"></p>
    <div id="droppable">
      <div id="d1" class="squaredotted-p1">Drop answer here</div>
      <div id="d2" class="squaredotted-p2">Drop answer here</div>
      <div id="d3" class="squaredotted-p3">Drop answer here</div>
      <div style="clear:both"></div>

      <p class="play"><span class = "feedbackAnswer">Drop snippets to create the correct answer</span></p>
      <p class="answerDrop">Attempt Number: <span class='drop-result'></span></p>
    </div>

  </div><!--end of snippets-->

<!-- User feedback popups -->

  <div>
    <h2 class="userFeedback"><span class = "feedbacktoanswer"></span></h2>
    <button type="button" class="next">reset</button> 
  </div>


  <div class="correctFeedback">
    <a href="#" class="closePopupCorrect">finish</a>
    <p class="right lower"><span class="correct-result"></span></p>
    <p class="lighten">Number of attempts: <span class="numbGo"></span></p>
    <p>Time taken: <span class="numbTime"></span> seconds</p>
  </div>

  <div class="wrongFeedback">
    <a href="#" class="closePopup">Try again</a>
    <a href="#" class="closePopupCorrect">finish</a>
    <p class="lower"><span class="correct-result"></span></p>
    <p class="lighten">Number of Attempts: <span class="numbGo"></span></p>
    <div style="clear:both"></div> 
  </div>
  <div style="clear:both"></div> 

<!-- main performance review popup -->

  <div class="finalReviewInfo">

    <?php

    if ($this->session->userdata('state') == "new_user"): ?>

    <div id = "exit">
      <br />
      <?php
      echo '<i class="fa fa-star fa-fw"></i><span class="bold">Question Number: </span>'. $currentQuestionNumber;
      ?>
      <form class="question" method="post" action=""  >

        <!-- need a while loop here for the length of the questions array -->
         <!-- $question count is the number of questions in database -->
        
        <?php
        $count = 1;
        $qcount = count($questionCount);

        while ($count <= $qcount)
        {

          if ($currentQuestionNumber == $count && $currentQuestionNumber != $qcount )
          {
            $nextNumb = $count + 1;

            echo '<i class="fa fa-arrow-right fa-fw"></i><span class="bold">Go to next question</span>';
            echo ' <input name="question_number" type="submit" value = ' . $nextNumb . ' >';
          }

        /*if it is the final question..*/
          elseif ($currentQuestionNumber == $count && $currentQuestionNumber == $qcount) {
           echo "<div><span class = 'change_status'><a href = '#'>finish process</a></span></div>";

        /* then we change the session 'userstate' data to 'play_question'. This is done with a javascript function*/
         }

         $count++;

       }

       ?>

  </form>
</div><!-- end of exit-->


<?php

elseif ($this->session->userdata('state') == "playQuestion"): ?>
<a href="#" class="exitPopup">Exit</a>
<?php endif; ?>

<!-- Main feedback information displayed to the view -->

<h2 class="emphasize lower">Correct Answer</h2>
<div class="correct-answers"><span class="correctAnswers"></span></div>
<h2 class="emphasize">Your Review</h2>
<p class="lighten">Number of goes : <span class="numbGo"></span></p>
<p class="lighten">Number of seconds: <span class="endTime"></span></p>
<p class="lighten">Your difficulty factor: <span class="qvalue"></span></p>
<p class="lighten">New Interval: <span class="newInterval"></span></p>
<p class="lighten">Todays date: <span class = "todaysDate"></span></p>
<h2 class="emphasize">New date review: <span class = "newDate"></span></h2>

</div>





<!-- ================  END OF MAIN HTML =====================-->

<script type="text/javascript" src="<?php echo base_url('assets/jquery/external/jquery/jquery.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/jquery/jquery-ui.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/jquery.ui.touch-punch.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/script.js'); ?>"></script>

<!-- Javascript -->
<script>
   /*Global variables*/
    var emptyArray = [];
    var timer;
    var mins = 0;
    var dropCounter = 0;
    var numbAttempts = 0;
    console.log(dropCounter, "drop counter global");
    var endTimeResult;
    //for review
    var n = "<?php echo $n; ?>";
    var oEF = "<?php echo $oEF; ?>";
    var LstInt = "<?php echo $LstInt; ?>";
    //date stuff
    var timestampDate;
    var timestampDateTest;
    var myDate;
    var dateForDatabase;
    var user_id = "<?php echo $session_id; ?>";
    var new_n;
    var EF;
    var q;
    var secs = $("#timeField").html();
    //global result changed upon whether ueser gets question right or wrong
    var userResult;

    $(document).ready(function () {
      console.log("ready!");
      Clock.resume();
      console.log(n, "n value");
      console.log(oEF, "current EF");
      console.log(LstInt, "current LstInt");
      console.log(secs);
      var test = "pause";
      var pause = true;

      /*pause button functionality*/
      $('#pause-butt').click( function() {
   

        if (pause == true){
          $("#pause-butt").attr("src", "assets/images/button_play.png");
          Clock.pause();
          $(".pauseSheet").show();
          pause = false;

        } else if (pause == false){

          $("#pause-butt").attr("src", "assets/images/button_pause.png"); 
          Clock.resume();
          $(".pauseSheet").hide();
          pause = true; 
        }

      } );

      /* reshuffle the dynamic divs so they appear randomly on each page reload */

      var div = $(".square").toArray();
      //randomly print them back out.
      while(div.length > 0) {
        var idx = Math.floor((Math.random() * (div.length-1)));
        var element = div.splice(idx, 1);
        $('div#draggable').append(element[0]);
      }

      }); 
/*end of document ready*/


   function pauseOption(){
    
    $("#pause-butt").attr("src", "assets/images/button_help.png");
    };

    $(".exitPopup").click(function () {

    window.location = "questionmenu";

  });

/** 
on question ten (final question) the finish button must change the session and open a new page.
change the state from new user to play quetion after initial question process
 */

  $(document).on('click','.change_status',function(){
 
   var playQuestion = "playQuestion";
   var sendData = {
    value: playQuestion 

  }
  $.when(
   $.ajax({

    url: "<?php echo base_url('play/sessions'); ?>",
    type: "POST",
    datatype: "json",
    data: sendData,
    success: function (data) {
      console.log(data, "saved to database");
      console.log(sendData);

    }

  })

   ).then(function() {

    window.location.href = 'newuser_complete';

  });

 });//end of change status


    $(".closePopup").click(function () {

        if (numbAttempts <= 2) {
            $(".wrongFeedback, .correctFeedback").hide("slide", {
                direction: "down"
            }, 1000);
            reset();
            Clock.resume();


        } else if (numbAttempts == 3) {

  
     
            $(".wrongFeedback, .correctFeedback").hide("slide", {
                direction: "down"
            }, 1000);
            resetCorrect();

          
        }


    });

    $(".closePopupCorrect").click(function () {

        $(".wrongFeedback, .correctFeedback").hide("slide", {
            direction: "down"
        }, 1000);

        resetCorrect();
    });


/*Update the counter*/
    function updateCounter() {

        dropCounter++;

    }

/*Reset draggables function*/
    function reset() {
        dropCounter = 0;

        $("#d2, #d3").hide();
        console.log(dropCounter, "drop counter inside reset");
        emptyArray = [];
        $("p.answerDrop").show();

        var attemptNumber = numbAttempts + 1;
        $(".drop-result").html(attemptNumber);


        $("#d1, #d2, #d3").droppable(({
            disabled: false
        }));


        //move snippets back to original place
        $("#draggable .square").css({
            'left': $("#draggable .square").data('originalLeft'),
            'top': $("#draggable .square").data('origionalTop')
        });
        $('.feedbackAnswer').html('Drop snippets to create the correct answer');

        $("#draggable .square").removeClass("wrongBg");


        if (numbAttempts == 3) {
            alert("game finished");

            Clock.pause();
            endTimeResult = secs;


            $("#draggable .square").addClass("disabled");

        }
        console.log(numbAttempts, "numb attempts on reset");

    }

  /**
 *This function is called when all three attempts have been made OR when the user gets the correct answer.
 *The function resets, stops the clock, disables the draggables, moves snippets back to original place,
 *calls/displays the review info section, and uses the current users data with the current data from the finished question
 *to calculate the nxt review date. This is displayed in the Review pop up and saved into the database.
 * 
 */


    function resetCorrect() {
            dropCounter = 0;
            console.log(dropCounter, "drop counter inside reset");
            emptyArray = [];
            Clock.pause();
            $(".closePopup").hide();
            $("#d1, #d2, #d3").droppable(({
                disabled: true
            }));

            //move snippets back to original place
            $("#draggable .square").css({
                'left': $("#draggable .square").data('originalLeft'),
                'top': $("#draggable .square").data('origionalTop')
            });
            $('.feedbackAnswer').html('Drop snippets to create the correct answer');
            $("#draggable .square").removeClass("correctBg");
            $("#draggable .square").removeClass("wrongBg");
            $("#draggable .square").addClass("disabled");

            /*show the final review pop up*/
            $(".finalReviewInfo").show("slide", {
                direction: "down"
            }, 1000);
            $( ".exitPopup" ).slideUp( 300 ).delay( 3500 ).fadeIn( 1000 );

            /*get the time for the puzzle completetion*/
            endTimeResult = $("#timeField").html();
            $(".endTime").html(endTimeResult);


          /** CALCULATE THE NEXT REVIEW. call getQ and DO review */
          
            q = getQ(endTimeResult, numbAttempts);
 
            $(".qvalue").html(q);

            if (n == 1) {

                oEF = 2.5; //initial value
                LstInt = 1; //initial value

                if (q < 4) {

                    EF = 2.5; //(reset);
                    newInterval = 1; //see it again the next day
                    new_n = 1; //go back to n being 1;

                } else if (q >= 4) {

                    EF = getEF(oEF, q); //work out the next interval
                    newInterval = getnewInt(LstInt, EF);
                    new_n = (parseInt(n)) + 1;
                }

                console.log(n, "n");
                console.log(EF, "new EF");
                console.log(LstInt, "LstInt");
                console.log(newInterval, "new interval");

            } else if (n > 1) {

                if (q < 2) {

                    EF = 2.5; //(reset);
                    newInterval = 1; //see it again the next day
                    new_n = 1; //go back to n being 1;

                } else if (q >= 2) {

                    //have 0EF and LstInt set globally 
                    EF = getEF(oEF, q);
                    newInterval = getnewInt(LstInt, EF);
                    new_n = (parseInt(n)) + 1


                }

                console.log(EF, "new EF");
                console.log(newInterval, "new interval");
                console.log(LstInt, "LstInt");
                console.log(n, "n");

            }

            $(".newInterval").html(newInterval);
            setTimeReview(newInterval);
            //insert new values into databse with ajax
            user_id = "<?php echo $session_id; ?>";
            var questionNumber = "<?php echo $questionNumTest; ?>"
            var sendData = {
                question: questionNumber,
                newInterval: newInterval,
                newEF: EF,
                n: new_n,
                qFactor: q,
                nextReview: dateForDatabase,
                user_id: user_id
                   
            }

            $.ajax({

                url: "<?php echo base_url('get_answer/saveQuestionData'); ?>",

                type: "POST",
                datatype: "json",
                data: sendData,
                success: function (data) {

                    console.log(data, "saved to database");
                    console.log(sendData);

                }

            });
           //end of alax

        } //end of correct reset();



    var Clock = {
        secs: 0,

        start: function () {
            var self = this;

            this.interval = setInterval(function () {
                self.secs++;
                $("#timeField").text(parseInt(self.secs));

            }, 1000);
        },

        pause: function () {
            clearInterval(this.interval);
            delete this.interval;
        },

        resume: function () {
            if (!this.interval) this.start();
        }
    };


    $('#pauseButton').click(function () {
        Clock.pause();
    });
    $('#resumeButton').click(function () {
        Clock.resume();
    });


/**** ALGORITHM ******/

/*Get the q factor (users score)*/

    function getQ(endTimeResult, numbAttempts) {
          
            if (numbAttempts == 1 && endTimeResult <= 10) {

                q = 5;
            } else if (numbAttempts == 1 && (endTimeResult > 10 && endTimeResult <= 25)) {

                q = 4;
                } else if (numbAttempts == 1 && (endTimeResult > 25 && endTimeResult <=40)) {

                q = 3;

            } else if (numbAttempts == 1 && endTimeResult > 40) {
            //too long taken and n will have to go to n = 1
                q = 1;  

            } else if (numbAttempts == 2 && endTimeResult <=25) {
                q = 3;
            } else if (numbAttempts == 2 && (endTimeResult > 25 && endTimeResult <=40)) {
                q = 2;

            } else if (numbAttempts == 2 && endTimeResult  >40) {
                //too long taken and n will have to go to normal interval
                q = 1;
                //if user gets it wrong on 3 attempts
            }else if(numbAttempts == 3 && userResult == "wrong"){
             q = 0;
               //if user gets it right on third attempt
            } else if (numbAttempts == 3 && endTimeResult <= 25) {
                q = 2;
              //this is likely to be trial and errors
            } else if (numbAttempts == 3 && endTimeResult > 25) {
                q = 1;

            } 

            return q;

        } //end of getQ


    //get the new EF value. 'q' is calculates in getQ function above.

    function getEF(oEF, q) {


            if (parseFloat(oEF) + (0.1 - (5 - q) * (0.08 + ((5 - q) * 0.02))) > 1.3) {

                EF = parseFloat(oEF) + (0.1 - (5 - q) * (0.08 + ((5 - q) * 0.02)));

            } else {

                EF = 1.3

            }

            return EF;

        } //end of EF


    //get the new interval value

    function getnewInt(LstInt, EF) {
            newInt = Math.round(LstInt * EF).toFixed();
            return newInt;

        } 


function addDays(date, days) {
      var result = new Date(date);
      result.setDate(date.getDate() + days);
      return result;
}


function addDays(currentDate, numb) {
        var result = new Date(currentDate);
        result.setDate(currentDate.getDate() + numb);

        var dd = result.getDate();
        var mm = result.getMonth() + 1;
        var y = result.getFullYear();
        var resultFormattedDate = dd + '-' + mm + '-' + y;
        return resultFormattedDate;
}


    function setTimeReview(newInterval) {
        //get current date

        var todaysDate = new Date();
        var currentDate = new Date();
        var numb = parseInt(newInterval);        
        var someFormattedDate = addDays(currentDate, numb);

        //format the date
        var dd = todaysDate.getDate();
        var mm = todaysDate.getMonth() + 1;
        var y = todaysDate.getFullYear();
        var FormattedtodaysDate = dd + '-' + mm + '-' + y;

        $(".todaysDate").html(FormattedtodaysDate);
        $(".newDate").html(someFormattedDate);  
        dateForDatabase = someFormattedDate;
        console.log(dateForDatabase, "dateForDatabase");

    }

/** 
*START THE DRAG AND DROP FUNCTIONALITY
*/

    $(function () {

        var correctanswer;
        $('.feedbackAnswer').html('Drop snippets to create the correct answer');
        $("#draggable .square").draggable({
            snap: ".squaredotted-p1, squaredotted-p2, squaredotted-p3",
            snapTolerance: '20',
            snapMode: "inner",
            revert: 'invalid',
            stack: "#draggable .square",
            start: function (event, ui) {

                $(this).addClass("movingDraggable")
                   
               
            },
            stop: function (event, ui) {
                $(this).removeClass("movingDraggable")
            }


        });

        $("#draggable .square").data({
            'originalLeft': $("#draggable .square").css('left'),
            'origionalTop': $("#draggable .square").css('top')
        });

        //the accept puts a condition on the draagabklr to disable the draggable
        $("#droppable .squaredotted-p1, .squaredotted-p2, .squaredotted-p3").droppable({
            tolerance: "pointer",

            accept: function () {
               
                return $('.feedbackAnswer').html() == 'Drop snippets to create the correct answer';
            },
            drop: function (event, ui) {

                console.log("drop")

                $(this).droppable(({
                    disabled: true
                  
                }));

                updateCounter();

                $(".dropcount").html(dropCounter);

                console.log(ui.draggable[0].id);
                var droppedValue = ui.draggable[0].id;
                emptyArray.push(droppedValue);
                console.log(dropCounter, "dropCounter from draggable");
               
                /*switch case dependant on counter of dropped snippet, wither 1, 2 or 3*/
                /*Also need to show and hide the placeholder snippets*/
                /*Once all the placeholders ahve been dropped into (cse 3), we can analyse the results*/

                switch (dropCounter) {
                case 1:
                    $("#d2").show();
                    $("#d3").hide();
                    $("#d3").css("display", "none")

                    break;

                case 2:

                    $("#d3").show();

                    break;

                case 3:

                    $(this)
                        .addClass("ui-state-highlight")
                        .find(".drop-result")
                        .html(numbAttempts);
                    numbAttempts++;
                    $(".numbGo").html(numbAttempts);

                    var questionNumber = "<?php echo $questionNumTest; ?>"
                    var sendData = {
                        question: questionNumber,
                        item1: emptyArray[0],
                        item2: emptyArray[1],
                        item3: emptyArray[2]
                    }

                    console.log(sendData);
                    console.log(numbAttempts, "numb attamets ");

                    $.ajax({

                            url: "<?php echo base_url('get_answer/getans'); ?>",
                            type: "POST",
                            datatype: "json",
                            data: sendData,
                            success: function (data) {

                                    //if data returns TRUE
                                    if (data) {
                                 
                                        $(".correctFeedback").show("slide", {
                                            direction: "down"
                                        }, 200);
                                         Clock.pause();
                                        $(".closePopupCorrect").show();
                                        //change value of global var 'userResult'
                                        userResult = "correct";
                                        endTimeResult = $("#timeField").html();
                                        $(".numbTime").html(endTimeResult);                                                                         
                                        $(".correct-result").html("<i class='fa fa-check-square fa-fw'></i>Correct Answer");
                                         var questionAnswerNumber = <?php echo $js_array; ?>;
                                        var vPool = "";
                                        jQuery.each(questionAnswerNumber, function (i, val) {
                                          
                                               vPool += "<span class = 'displayAns'>" + val + "</span><br /><br />";
                                        });

                                        $("#draggable .square").addClass("correctBg");                                
                                        $('.correctAnswers').html(vPool);                                     

                                    } else {
                                        
                                        $('p.feedbackAnswer').html('wrong');                                 
                                        $(".wrongFeedback").show("slide", {
                                            direction: "up"
                                        }, 200);
                                        Clock.pause();
                                        //change value of global var userResult
                                        userResult = "wrong";
                                        $(".closePopup").show();
                                        //show correct answer
                                        $(".correct-result").html("Incorrect Answer");
                                        var questionAnswerNumber = <?php echo $js_array; ?>;;
                                        var vPool = "";
                                        jQuery.each(questionAnswerNumber, function (i, val) {
                                             vPool += "<span class = 'displayAns'>" + val + "</span><br /><br />";                                         
                                          
                                        });
                                                                            
                                        $('.correctAnswers').html(vPool);
                                       
                                        $("#draggable .square").addClass("wrongBg");

                                    } //end of else

                                } //end of if

                        }) //end of ajax!!

                    break;

                default:

                    $("#d1").show();

                } //end of switch

            }

        }); //end of droppable

    }); //end of function
</script>
</body>
</html>

