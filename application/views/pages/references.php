<div class="menu-header">
	<h1>References</h1>
</div>

	<div class = "references">

		<div class="menu-links-alt">
			<ul>
				<?php echo anchor('http://www.supermemo.com','<li>Super memo introduction</li>', array('title' => 'Super memo introduction', 'target' => '_blank'));?>
				<?php echo anchor('http://www.supermemo.com/articles/20rules.htm','<li>Twenty rules of formulating knowledge</li>', array('title' => 'Twenty rules of formulating knowledge', 'target' => '_blank'));?>
				<?php echo anchor('http://www.supermemo.com/english/ol/sm2.htm','<li>SM-2 Algorithm</li>', array('title' => 'SM-2 Algorithm','target' => '_blank' ));?>
				<?php echo anchor('http://www.supermemo.com/english/princip.htm','<li>Super memo - the problem of forgetting</li>', array('title' => 'the problem of forgetting','target' => '_blank'));?>
				<?php echo anchor('http://lifeinthefastlane.com/learning-by-spaced-repetition/','<li>Learning by Spaced Repetition</li>', array('title' => 'Learning by Spaced Repetition','target' => '_blank'));?>
				<?php echo anchor('http://crpit.com/confpapers/CRPITV52Parsons.pdf','<li>Parsons Puzzles</li>', array('title' => 'Parsons Puzzles','target' => '_blank'));?>
				<?php echo anchor('http://archive.wired.com/medtech/health/magazine/16-05/ff_wozniak?currentPage=all','<li>Wozniak interview</li>', array('title' => 'Wozniak interview','target' => '_blank'));?>
				<?php echo anchor('http://www.gwern.net/docs/spacedrepetition/1989-dempster.pdf','<li>Implications for theory and practice</li>', array('title' => 'Spacing Effects and their implications for theory and practice','target' => '_blank'));?>
				<?php echo anchor('http://andrewvs.blogs.com/usu/files/the_spacing_effect.pdf','<li>The Spacing Effect</li>', array('title' => 'The Spacing Effect','target' => '_blank'));?>

			</ul>
		</div>
		<div class="menu-links-alt addPaddingTop">
			<ul>
				<?php echo anchor('instructions','<li>Instructions</li>', array('title' => 'Instructions'));?>
			</ul>
		</div>
	</div>







