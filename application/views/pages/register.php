<div class="menu-header">
	<h1>Sign up</h1>
</div>
<div class="snippets">

  <div id="content">
    <div class="signup_wrap">

    </div>
  </div>
  <div class="reg_form">


   <?php echo validation_errors('<p class="logErrors">'); ?>
   <?php $attributes = array('class' => 'contact-form');
   echo form_open('', $attributes);?>

  <p>
    <label for="email_address">Your Email:</label>
    <input type="text" id="email_address" name="email_address" value="<?php echo set_value('email_address'); ?>"  required />
  </p>
  <p>
    <label for="password">Password:</label>
    <input type="password" id="password" name="password" value="<?php echo set_value('password'); ?>" required />
  </p>
  <p>
    <label for="con_password">Confirm Password:</label>
    <input type="password" id="con_password" name="con_password" value="<?php echo set_value('con_password'); ?>" required />
  </p>
  <p>
    <input type="submit" class="greenButton" value="Submit" />
  </p>
  <?php echo form_close(); ?>
</div>
</div>




