<div class="menu-header">
	<h1>Contact us</h1>
</div>
<div class="snippets">


    <!--echo out the flashdata 'errors' if it is not blank-->
         <div class = "emailflashdata"> 
            <?php if ($this->session->flashdata('errors') != ''): 
            echo $this->session->flashdata('errors'); 
            endif; ?>
          </div> 
             <div class = "emailflashdata"> 
            <?php
            if ($this->session->flashdata('message') != ''): 
            echo $this->session->flashdata('message'); 
            endif; ?>
            </div> 
    <!--add attributes to form in array-->
        <?php $attributes = array('class' => 'contact-form', 'name' => 'contact-form'); ?>
        <?php echo form_open('contact/sendmail',$attributes ); ?>


    <p class="name"> 
        <label for="name">Name</label> 
        <input type="text" name="name" id="name" placeholder="john doe" required="required" /> 
             <?php echo form_error('name', '<div class="logErrors">', '</div>'); ?>
    </p> 
   
    <p class="email"> 
        <label for="email">E-mail</label> 
        <input type="text" name="email" id="email" placeholder="johndoe@hotmail.com" required="required" /> 
        <?php echo form_error('email', '<div class="logErrors">', '</div>'); ?>
    </p> 
   
    <p class="text"> 
      <label for="message">Message</label> 
        <textarea name="message" required="required"></textarea> 
             <?php echo form_error('message', '<div class="logErrors">', '</div>'); ?>
       
    </p> 
   
    <p class="submit"> 
        <input type="submit" value="Send" /> 
    </p> 
   
<?php echo form_close();?>


</div>
