<div class="menu-header">
	<h1>Congratulations</h1>
</div>
<div class="snippets">
	<div class="menu-links">
		<p class="expanded"><i class="fa fa-thumbs-up fa-fw"></i>You are now ready to start doing your rehearsals</p>
		<p class="expanded">Go to the <span class="bold">question menu</span> and view the dates for each question review</p>
		<p class="expanded bold"><i class="fa fa-info-circle fa-fw"></i>A question can only be viewed when it is ready for rehearsal</p>
		<p class="expanded">You will receive an email when a question is due for rehearsal</p>

		<ul>
			<?php echo anchor('questionmenu','<li class="box-menu">question menu</li>', array('title' => 'question menu'));?>
		</ul>
		<p class="emphasize expanded">Let your learning experience begin...</p>

	</div>

</div>
