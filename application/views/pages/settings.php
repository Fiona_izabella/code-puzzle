<div class="menu-header">
  <h1>Settings</h1>
</div>
<div class="snippets">
  <div class="menu-links">

 <img src="assets/images/audioinprogress.png">
    <p class="expanded">Select audio options for puzzle game</p>

    <input type="radio" id="" name="audio" value="all">
    <label for="radio3">Audio on</label>
    <input type="radio" id="" name="audio"value="false">
    <label for="radio4">Audio off</label>

     <?php $data = array(
          'type'        => 'submit',
          'name'       => 'submitAudio',
          'id'          => 'change-audio',
          'class'       => 'change-audio',
          'value'       => 'work in progress',
          'disabled' => 'disabled'
        );

     echo form_input($data);?>


<p class="expanded">Enable email notifications</p>

<!-- radio buttons for setting email notification preferences -->

<?php echo form_open(''); ?>

   <?php if(checkRadioState()):?>
    <input type="radio" id="radio5" name="emails" value="true" checked>
    <label for="radio5">email on</label>
    <input type="radio" id="radio6" name="emails" value="false">
    <label for="radio6">email off</label>

  <?php else  :?>
     <input type="radio" id="radio5" name="emails" value="true" >
    <label for="radio5">email on</label>
    <input type="radio" id="radio6" name="emails" value="false" checked>
    <label for="radio6">email off</label>

  <?php endif;?>


      <?php $data = array(
          'type'        => 'submit',
          'name'       => 'mysubmit',
          'id'          => 'change-emailNotification',
          'class'       => 'change-emailNotification',
          'value'       => 'submit change'
        );

     echo form_input($data);
    echo form_close(''); ?>

    <p class="expanded">Customise skin</p>


<?php if(isset($_COOKIE['pagecontrast']) && ($_COOKIE['pagecontrast'] == 'contrast')): ?>

  <!-- radio buttons for setting customise skin preferences -->

    <?php echo form_open(''); ?>
    <input type="radio" id="radioskin1" name="pagestyle" value="normal">
    <label for="radioskin1">original</label>
    <input type="radio" id="radioskin3" name="pagestyle" value="contrast" checked>
    <label for="radioskin3">dark</label>
    
    <?php $data = array(
          'type'        => 'submit',
          'name'       => 'mysubmit',
          'id'          => 'change skin',
          'class'       => 'change-skin',
          'value'       => 'submit skin'
        );

     echo form_input($data);
     echo form_close(''); ?>

    <?php else: ?>


        <?php echo form_open(''); ?>
    <input type="radio" id="radioskin1" name="pagestyle" value="normal" checked>
    <label for="radioskin1">original</label>
    <input type="radio" id="radioskin3" name="pagestyle"value="contrast">
    <label for="radioskin3">dark</label>
   
    <?php $data = array(
          'type'        => 'submit',
          'name'       => 'mysubmit',
          'id'          => 'change skin',
          'class'       => 'change-skin',
          'value'       => 'submit change'
        );

     echo form_input($data);
  
    echo form_close(''); ?>

<?php endif; ?>



  </div>

</div>

