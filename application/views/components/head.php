<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Code Puzzle</title>
	<meta name="description" content="Puzzle game to aid in teh memorising of programming code syntax">
	<meta name="author" content="Fiona Przybylski">

	<link href="<?php echo base_url('assets/normalize.css'); ?>" rel="stylesheet"> 
	<link href="<?php echo base_url('assets/jquery/jquery-ui.min.css'); ?>" rel="stylesheet"> 
	<link href="<?php echo base_url('assets/font-awesome-4.1.0/css/font-awesome.min.css'); ?>" rel="stylesheet"> 

	  <!-- my styles depending on the cookie value-->
  <?php if(isset($_COOKIE['pagecontrast']) && ($_COOKIE['pagecontrast'] == 'contrast')) :?>
    <link href="<?php echo site_url('assets/style_dark.css'); ?>" rel="stylesheet">
  <?php  else: ?>
    <link id="stylesheet" href="<?php echo site_url('assets/style.css'); ?>" rel="stylesheet">
  <?php endif; ?>
<!--  modernizr library -->
  <script type="text/javascript" src="<?php echo base_url('assets/jquery/modernizr.js'); ?>"></script>

</head>
<body>

<div class="page-wrapper">










    