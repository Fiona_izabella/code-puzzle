<?php $slug = $this->uri->segment(1);?>
<nav>
	<div class="nav-wrapper">
		<ul class="nav-list clearfix">
			<li><a href="menu"><img src="assets/images/button_home.png" alt="back to home link"></a></li>
			<?php if ($slug=="progress"): ?>
				<li><a href="progress"><img src="assets/images/button_progress-active.png" alt="progress" id=""></a></li>
			<?php else: ?>
				<li><a href="progress"><img src="assets/images/button_progress.png" alt="progress" id=""></a></li>
			<?php endif; ?>
			<li><a href="#"><img src="assets/images/button_help.png" alt="help search drop down" id="opener"></a></li>
			<li><a href="login/logout"><img src="assets/images/button_cross.png" alt="log out" title="log out"></a></li>
			<li id = "pauseToggler"><a href="#"><img src="assets/images/button_audio.png" alt="back to home link"><img src="assets/images/button_audio_blue.png" style="display:none"/></a></li>		
		</ul>
	</div>
	
</nav>


<div id="dialog" title="Help Search">

	<form id="live-search" action="" class="styled" method="post">
		<fieldset>
			<input type="text" class="text-input" id="filter" value="" />
			<span id="filter-count"></span>
		</fieldset>
	</form>

	<dl class="accordion">

		<dt><a href="">New user</a></dt>
		<dd>After registering, you log in as a new user and need to go through the initial process of 
			carrying out the questions. You need to follow the process <span class ="bold">step one -> step 
			three</span> which leads to the loading of your questions. You then go through each question. 
			After each question you will be shown a review of your question and asked to 
			select <span class ="bold">continue</span>. Once all questions are finished, 
			you will be asked to exit and you will be ready to start your repetitions.
		</dd>

		<dt><a href="">Rehearsal time</a></dt>
		<dd>Log in to the application, <span class ="bold">select play->load your questions</span> and you will be in the <span class ="bold">questions menu page</span>.
			You will find each question with its review date. Select the question that is ready for rehearsal.
			You may only view a question when it is ready for rehearsal.</dd>

			<dt><a href="">Missing a rehearsal</a></dt>
			<dd>If you do not complete your question within 5 days, you will need to reset the question. 
				It will then be set to that day, allowing you a range of 5 days for completeion.
				However the algorithm will be set back to default, and be treated as a new question</dd>

				<dt><a href="">Getting rehearsal dates</a></dt>
				<dd>You can check if your question is ready for review in the <span class ="bold">question menus page</span>.
					Code puzzle will also send you an email notfication when your question is ready for viewing. You have 5 days within which to complete the rehearsal.</dd>

					<dt><a href="">Registering</a></dt>
					<dd>To register just select the <span class ="bold">register option</span> on the home log in page. This will take you
						to a registration page where you can add your details.</dd>

						<dt><a href="">Playing the puzzle</a></dt>
						<dd>You can load a question through the <span class ="bold">questions menu</span> page. Once the question loads, the timer is started and you are
							ready to drag the code snippets into the three placeholders below in order to formulate the correct answer. You are
							given three attempts to get the correct answer. Once you get the correct answer OR have used up your three atempts, you
							will receive a review of your perfomrance and your next rehearsal date.</dd>

							<dt><a href="">Contacting us</a></dt>
							<dd>You can make an online query through our Contact page. Your questions
								will be promptly answered and your feedback and opinions are valuable to our research.</dd>

								<dt><a href="">How can I review my performace</a></dt>
								<dd>Your <span class ="bold">progress page</span> contains a progress graph for each of your questions. You can also choose to view your progress in a drop down table.</dd>


							</dl>

						</div>
