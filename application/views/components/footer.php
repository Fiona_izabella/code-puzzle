<footer>
  <p>copyright &copy;fionawebdesign</p>
</footer>
 </div><!--end of page wrapper-->



<!--javascript-->
<script type="text/javascript" src="<?php echo base_url('assets/jquery/external/jquery/jquery.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/jquery/jquery-ui.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/jquery.ui.touch-punch.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/script.js'); ?>"></script>
<!-- Javascript -->
<script type="text/javascript">


/*button which changes the users state in the session to a 'new_user"*/

$('a.newUser').click(function(){

  console.log("new_user state");

  $('li.userstate-b').addClass('selected');
  $('li.userstate-a').removeClass('selected');
  $('.menu-links ul li#play-ready').show();

  var playQuestion = "new_user";

  var sendData = {
    value: playQuestion 

  }

  $.ajax({

    url: "<?php echo base_url('play/sessions'); ?>",
    type: "POST",
    datatype: "json",
    data: sendData,
    success: function (data) {

      console.log(data, "saved to database");
      console.log(sendData);

    }

  })

});

/*button which changes the users state in the session to a returning user  'play_Question"*/
$('a.playQuestion').click(function(){

  console.log("playquestion state");

  $('li.userstate-a').addClass('selected');
  $('li.userstate-b').removeClass('selected');
  $('.menu-links ul li#play-ready').show();

  var playQuestion = "playQuestion";

  var sendData = {
    value: playQuestion 

  }

  $.ajax({

    url: "<?php echo base_url('play/sessions'); ?>",
    type: "POST",
    datatype: "json",
    data: sendData,
    success: function (data) {

      console.log(data, "saved to database");
      console.log(sendData);

    }

  });


});


/*button which sets a value to put into the resetMissedRehearsal() function and calls the function*/
  
$('a li.altButton').click(function(){

  var currentQuestionNumber = $( this ).html();
  //var getNumbersection = currentQuestionNumber.substr(15, 1);
  //get last 2 values of string, eg 9, or 10
  var getNumbersection = currentQuestionNumber.substr(-2);

  console.log(getNumbersection);
  resetMissedRehearsal(getNumbersection);
});


/*function which resets a missed rehearsal back to its original value*/
function resetMissedRehearsal(passedNumber){

  var user_id = "<?php echo $this->session->userdata('id') ?>";
  var oEF = 2.5; 
  var LstInt = 1; 
  var EF = 2.5;
  var newInterval = 1;
  var qFactor = 1;
  var new_n = 1;
  var todaysDate = new Date();
  var dd = todaysDate.getDate();
  var mm = todaysDate.getMonth() + 1;
  var y = todaysDate.getFullYear();
  var todaysFormattedDate = dd + '-' + mm + '-' + y;

  var questionNumber = passedNumber;


  var sendData = {
    question: questionNumber,
    newInterval: newInterval,
    newEF: EF,
    n: new_n,
    nextReview: todaysFormattedDate,
    user_id: user_id,
    qFactor: qFactor

  }

  $.ajax({

    url: "<?php echo base_url('get_answer/saveQuestionData'); ?>",
    type: "POST",
    datatype: "json",
    data: sendData,
    success: function (data) {

      console.log(data, "saved resetting question to database");
      console.log(sendData);
      location.reload();


    },
    error: function(xhr, status, error) {
                    // Display Ajax error
                    alert("AJAX Error!");
                  }

                });

}


/*pause button to toggle the pause image*/

  $("#pauseToggler").click(function() {
    $(this).find('img').toggle();
  });


/*radio button functionality for changing the users email notification status*/

  $(function() {   
           $(".button-email").click(function() {  
           // validate and process form here
           var radioemail;

           if ($("input[name='emails']:checked").length > 0){
               radioemail = $('input:radio[name=emails]:checked').val();
           }
           else{
               alert("No button selected, try again!");
               return false;
           }

           var sendData = {
            value: radioemail 

            }

           $.ajax({
             url: "<?php echo base_url('get_answer/changeEmailNotification'); ?>",
               type: "POST", 
                datatype: "json",
               data: sendData, 
               success: function() { 
                   // alert("form submitted: "+ radioemail);
                    console.log(sendData);
               },
             
            });
            return false;
         });
});


</script>
</body>
</html>