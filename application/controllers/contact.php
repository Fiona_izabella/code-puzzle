<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {


	public function __construct()
	{
		parent::__construct();

		$this->load->model('');
		$this->load->library('session');

	}


	public function index()
	{
		$this->data['subview'] = 'contact';
		$this->load->view('layout_puzzle', $this->data);
	}


	public function sendmail(){

		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name','trim|required');
		$this->form_validation->set_rules('email', 'Email','trim|required|valid_email');
		$this->form_validation->set_rules('message', 'Message','trim|required');

		if($this->form_validation->run() == FALSE){


			$this->session->set_flashdata('errors', validation_errors());

			redirect('contact');


			echo "failed";

		} else{

            	  //validation has passed and send email
            $name = $this->input->post('name');//same as $_POST['name'];
            $email = $this->input->post('email');
            $contactmessage = $this->input->post('message');
            $message = "This was sent from $name <br/>";
            $message .= "Their email is $email <br/>";
            $message .= "The message is $contactmessage!<br/>";


             //set the configuration for sending an email       
            $config = array(
            	'protocol'  =>  'smtp',
            	'smtp_host' =>  'ssl://smtp.gmail.com',
            	'smtp_user' =>  'fionka78@googlemail.com',
            	'smtp_pass' =>  'webmistress78',
            	'smtp_port' =>  '465',
            	'mailtype' => 'html',
            	'charset'   => 'iso-8859-1'
            	);

            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from($email, $name);
            $this->email->subject('code puzzle contact form');
            $this->email->to('fionka78@hotmail.com');

            $this->email->message($message);

            if($this->email->send()){
            	$this->session->set_flashdata('message', "Thankyou $name, your email has been sent");
            	redirect('contact');

            }else{

            	//show_error($this->email->print_debugger());
            	echo "your email didnt send";
            }

        }

    }



}//end of class