<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
class Registration extends CI_Controller

{


  public function __construct()
  {
    parent::__construct();

    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->model('user_model');


  }

  public function index()
  {

//if user already signed up and is logged in then redirect to menu
   if ( $this->session->userdata('logged_in')==TRUE)
   {
    redirect('menu');
  }


  $this->form_validation->set_rules('email_address', 'Your Email', 'trim|required|valid_email|unique[users.email]');
  $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
  $this->form_validation->set_rules('con_password', 'Password Confirmation', 'trim|required|matches[password]');

  if($this->form_validation->run() == FALSE)
  {
    $this->data['subview'] = 'register';
    $this->load->view('layout_puzzle', $this->data);
  }
  else
  {

    //add users to users table
    $this->user_model->add_user();
        

    redirect('login_newuser', 'refresh');

  }


}


}
?>