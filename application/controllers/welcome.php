<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {


	public function __construct()
	{
		parent::__construct();

		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->model('user_model');

	}


	public function index()
	{

//if user already signed up and is logged in then redirect 
		if ( !$this->session->userdata('logged_in')==TRUE)
		{
			redirect('login_newuser');
		}

		$this->data['subview'] = 'welcome';
		$this->load->view('layout_puzzle', $this->data);
	}


	public function add_table(){
  	//if successful i.e data base is created, user can carry on to next step and carry out questions.

		$this->load->dbforge();
		$this->load->helper('date');

		$user_id = $this->session->userdata('id');
		$table_name = 'user_'.$user_id;

//if table exists then redirect to newuser_play so user can start playing questions.
		if ($this->db->table_exists($table_name)){
			redirect('newuser_play');

		}

		$fields = array(
			'question_id' => array(
				'type' => 'INT',
				'constraint' => 11
				/* 'auto_increment' => TRUE*/
				),

			'n' => array(
				'type' => 'INT',
				'constraint' => 11
				),

			'newInt' => array(
				'type' => 'INT',
				'constraint' => 11
				),

			'EF' => array(
				'type' => 'FLOAT',
				'constraint' => 11
				),

			'previous_qValue' => array(
				'type' => 'FLOAT',
				'null' => TRUE
				),

			'nextReview' => array(
				'type' => 'DATETIME'
				),

			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11
				),
			);


		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('question_id', TRUE);

  //need to get the users id number to create the table
		if ($this->dbforge->create_table($table_name))

		{
			$this->initiate_table();

		}


  }//end of add table function

  function initiate_table(){

  	$this->load->dbforge();
  	$this->load->helper('date');

  	$user_id = $this->session->userdata('id');
  	$table_name = 'user_'.$user_id;

  	$data=array(

  		array(
  			'question_id' => '1',
  			'n' => '1',
  			'newInt' => '1',
  			'EF' => '2.5',
  			'previous_qValue' => '2.5',
  			'nextReview' => now(),
  			'user_id' => $user_id

  			),

  		array(
  			'question_id' => '2',
  			'n' => '1',
  			'newInt' => '1',
  			'EF' => '2.5',
  			'previous_qValue' => '2.5',
  			'nextReview' => now(),
  			'user_id' => $user_id

  			),

  		array(
  			'question_id' => '3',
  			'n' => '1',
  			'newInt' => '1',
  			'EF' => '2.5',
  			'previous_qValue' => '2.5',
  			'nextReview' => now(),
  			'user_id' => $user_id

  			),

  		array(
  			'question_id' => '4',
  			'n' => '1',
  			'newInt' => '1',
  			'EF' => '2.5',
  			'previous_qValue' => '2.5',
  			'nextReview' => now(),
  			'user_id' => $user_id

  			),

  		array(
  			'question_id' => '5',
  			'n' => '1',
  			'newInt' => '1',
  			'EF' => '2.5',
  			'previous_qValue' => '2.5',
  			'nextReview' => now(),
  			'user_id' => $user_id

  			),

  		array(
  			'question_id' => '6',
  			'n' => '1',
  			'newInt' => '1',
  			'EF' => '2.5',
  			'previous_qValue' => '2.5',
  			'nextReview' => now(),
  			'user_id' => $user_id

  			),

  		array(
  			'question_id' => '7',
  			'n' => '1',
  			'newInt' => '1',
  			'EF' => '2.5',
  			'previous_qValue' => '2.5',
  			'nextReview' => now(),
  			'user_id' => $user_id

  			),

  		array(
  			'question_id' => '8',
  			'n' => '1',
  			'newInt' => '1',
  			'EF' => '2.5',
  			'previous_qValue' => '2.5',
  			'nextReview' => now(),
  			'user_id' => $user_id

  			),

  		array(
  			'question_id' => '9',
  			'n' => '1',
  			'newInt' => '1',
  			'EF' => '2.5',
  			'previous_qValue' => '2.5',
  			'nextReview' => now(),
  			'user_id' => $user_id

  			),

  		array(
  			'question_id' => '10',
  			'n' => '1',
  			'newInt' => '1',
  			'EF' => '2.5',
  			'previous_qValue' => '2.5',
  			'nextReview' => now(),
  			'user_id' => $user_id

  			)

  		);



$this->db->trans_start();
foreach ($data as $d) {
	$insert_query = $this->db->insert_string($table_name, $d);
	$insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
	$this->db->query($insert_query);
}
$this->db->trans_complete();

$this->resetDates($table_name);


redirect('newuser_play');


}


function resetDates($table_name){

	
	$currect_date = date('Y-m-d H:i:s');
	$data=array(
		'nextReview' => $currect_date


		);


	$questionNumbers = array('1', '2', '3', '4', '5','6','7','8','9','10');
	$this->db->where_in('question_id', $questionNumbers);

	$this->db->update($table_name, $data); 



}



}//end of class