<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Questionmenu extends CI_Controller {


  public function __construct()
  {
    parent::__construct();

    $this->load->model('my_classes');
    $this->load->helper('checkDates_helper');
    $this->load->helper('questionexist_helper');
    
  }


  public function index($questionNumTest = 1)
  {

    if (! $this->session->userdata('logged_in')==TRUE)
    {
      redirect('login');
    }

    if ($this->session->userdata('state') == "new_user")
    {
      redirect('welcome');
    }


    if ($this->input->post('question_number')) {
     $questionNumTest = $this->input->post('question_number');


   }

   $this->data['questionNumTest'] = $questionNumTest;
   $this->data['codePiecesArray'] = $this->my_classes->getSnippets($questionNumTest);
   $this->data['codeTextAnswers'] = $this->my_classes->getSnippetAnswers($questionNumTest);
   $this->data['codePiecesIds'] = $this->my_classes->getsnippetsids($questionNumTest);
   $this->data['getQuestion'] = $this->my_classes->getQuestion($questionNumTest);
   $this->data['getAnsPrint'] = $this->my_classes->getanswerForprint($questionNumTest);
   $this->data['getQuestionInfo'] = $this->my_classes->getQuestionInfo($questionNumTest);
   $this->data['currentQuestionNumber'] = $questionNumTest;
   $this->data['questionReviewDate'] = $this->my_classes->getReviewDate();
   $this->data['questionCount'] = $this->my_classes->getQuestionCount();

   $this->data['subview'] = 'questionmenu';
   $this->load->view('layout_puzzle', $this->data);
 }
}
