<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Get_answer extends CI_Controller {


  public function __construct()
  {
    parent::__construct();

  }

/**
 *Function which calculates if the answer is correct or wrong
 *Function called through AJAX and returns a boolean value
 */
function getans(){

  $quetionID = $_POST["question"];
  $answer1 = $_POST["item1"];
  $answer2 = $_POST["item2"];
  $answer3 = $_POST["item3"];

  $this->db->select('answer1, answer2, answer3');
  $this->db->where('id', $quetionID);
  $query = $this->db->get('question');
  $answerSequence = $query->result_array(); 


  if ( $answerSequence[0]["answer1"] == $answer1 && $answerSequence[0]["answer2"] == $answer2 && $answerSequence[0]["answer3"] == $answer3 )
    echo true;
  else echo false;

}



function sessions($value){
 $this->session->set_userdata('state', $value);
}



/**  
 * Save users performace on a question to the database. Important function
 */

function saveQuestionData(){

  $questionID = $_POST["question"];
  $newInterval = $_POST["newInterval"];
  $n = $_POST["n"];
  $newEF = $_POST["newEF"];
  $qFactor = $_POST["qFactor"];
  $nextReview = $_POST["nextReview"];
  $user_id = $this->session->userdata('id');
  $table_name = 'user_'.$user_id;

//update users question table
  $this->db->set('n', $n);
  $this->db->set('newInt', $newInterval);
  $this->db->set('EF', $newEF);
  $this->db->set('user_id', $user_id);
  $this->db->set('previous_qValue', $qFactor);
  $this->db->set('nextReview',  date('Y-m-d', strtotime($nextReview)));
  $this->db->where('question_id', $questionID);
  $this->db->update($table_name);
  if ($this->db->_error_message());

//update 'users' table
  $this->db->set('question_id', $questionID);
  $this->db->set('nextReview',  date('Y-m-d', strtotime($nextReview)));
  $this->db->where('user_id', $user_id);
  $this->db->update('users');
  if ($this->db->_error_message());


//insert value into review table
  $data = array(
   'user_id' => $user_id ,
   'question_id' => $questionID,
   'n_value' => $n,
   'q' => $qFactor,
   'nextReview' => date('Y-m-d', strtotime($nextReview))
   );

  $this->db->insert('review ', $data); 
  if ($this->db->_error_message());


}//end of saveQuestionData()


}//end of class
?>