<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {


  public function __construct()
  {
    parent::__construct();

    $this->load->model('');
    $this->load->helper('radio_helper');

    //set cookies for the page style
   $value = $this->input->post('pagestyle');
   
    if( isset($value) && $value == 'contrast'){
     setcookie('pagecontrast', 'contrast', time()+3600);//expires in one hour
     redirect(current_url());
     
    
   }  else if (isset($value) && $value == 'normal'){
     setcookie('pagecontrast', 'contrast', time()-3600);//delete cookie
     redirect(current_url());
     

   }

 }


 public function index()
 {

  $user_id = $this->session->userdata('id');

  /*set email notifications in database depending on radio button value*/
  if(isset($_POST['mysubmit'])){
    $notification = $_POST['emails'] == 'true' ? true : false;
    $this->db->set('notifications', $notification);
    $this->db->where('user_id', $user_id);
    $this->db->update('users');
    if ($this->db->_error_message());
  }

  $this->data['subview'] = 'settings';
  $this->load->view('layout_puzzle', $this->data);

}




}//end of class