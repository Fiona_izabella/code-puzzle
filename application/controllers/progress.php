<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Progress extends CI_Controller

  {
  public

  function __construct()
    {
    parent::__construct();
    $this->load->model('my_classes');
    $this->load->model('qReviewResults');
    $this->load->helper('questionexist_helper');
     $this->load->helper('checkDates_helper');

    }

  public

  function index($question_number = 1)
    {

       if (! $this->session->userdata('logged_in')==TRUE)
      {
      redirect('login');
      }

      if ($this->session->userdata('state') == "new_user")
    {
    redirect('welcome');
    }
      
    if ($this->input->post('question_number'))
      {
      $question_number = $this->input->post('question_number');
      }

    $user_id = $this->session->userdata('id');
    $this->data['questionNum'] = $question_number;
    $this->data['all_q_values'] = $this->my_classes->getQuestionsProgress($user_id);
    $this->data['questionReviewDate'] = $this->my_classes->getReviewDate();
    $this->data['getUsersNextReview'] = $this->qReviewResults->getUsersNextReview($user_id);
    $this->data['getUserNextReviewq1'] = $this->qReviewResults->getUserNextReviewq1($user_id);
    $this->data['getUserNextReviewq2'] = $this->qReviewResults->getUserNextReviewq2($user_id);
    $this->data['getUserNextReviewq3'] = $this->qReviewResults->getUserNextReviewq3($user_id);
    $this->data['getUserNextReviewq4'] = $this->qReviewResults->getUserNextReviewq4($user_id);
    $this->data['getUserNextReviewq5'] = $this->qReviewResults->getUserNextReviewq5($user_id);
    $this->data['getUserNextReviewq6'] = $this->qReviewResults->getUserNextReviewq6($user_id);
    $this->data['getUserNextReviewq7'] = $this->qReviewResults->getUserNextReviewq7($user_id);
    $this->data['getUserNextReviewq8'] = $this->qReviewResults->getUserNextReviewq8($user_id);
    $this->data['getUserNextReviewq9'] = $this->qReviewResults->getUserNextReviewq9($user_id);
    $this->data['getUserNextReviewq10'] = $this->qReviewResults->getUserNextReviewq10($user_id);
    $this->data['questionCount'] = $this->my_classes->getQuestionCount();

    $this->data['subview'] = 'progress';
    $this->load->view('layout_puzzle', $this->data);
    }
  } //end of class