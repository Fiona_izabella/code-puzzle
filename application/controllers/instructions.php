<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Instructions extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('');
                   
    }
    
    
    public function index()
    {
        $this->data['subview'] = 'instructions';
        $this->load->view('layout_puzzle', $this->data);
    }
       
    
} //end of class