<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
class Userstatus extends CI_Controller

  {
  public

  function __construct()
    {
    parent::__construct();
    $this->load->model('');
    }

  public

  function index()
    {
    $this->data['subview'] = 'user-status';
    $this->load->view('layout_puzzle', $this->data);
    }

  function sessions()
    {
    $value = $_POST["value"];
    $this->session->set_userdata('state', $value);
    }
  } //end of class