<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
class Newuser_complete extends CI_Controller

  {
  public

  function __construct()
    {
    parent::__construct();
    $this->load->model('');
    }

  public

  function index()
    {
    $this->data['subview'] = 'newuser_complete';
    $this->load->view('layout_nonav', $this->data);
    }

  function sessions()
    {
    $value = $_POST["value"];
    $this->session->set_userdata('state', $value);
    }
  } //end of class