<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newuser_play extends CI_Controller {


  public function __construct()
  {
    parent::__construct();
    
    
    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->model('user_model');
    

    
  }


  public function index()
  {

//if user already signed up and is logged in then redirect to menu
   if ( !$this->session->userdata('logged_in')==TRUE)
   {
    redirect('login_newuser');
  }

  $this->data['subview'] = 'newuser_play';
  $this->load->view('layout_puzzle', $this->data);
}






}//end of class