<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller

{
  public

  function __construct()
  {
    parent::__construct();

    // need to load the session to use flashdata and add the user to teh session
    // Need to set up an encryption key in config

    $this->load->library('session');
    $this->load->model('user_data');
    session_start();
  }

  function index()
  {

    // Set up the validation

    $this->load->library('form_validation');
    $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
    if ($this->form_validation->run() == FALSE)
    {

      // if validation failed user is redirected to login page

      $this->data['subview'] = 'login';
      $this->load->view('layout', $this->data);
    }
    else
    {

      // Go to private area. redirect to menu
      // 
      redirect('menu', 'refresh');

    }
  }

  function check_database()
  {

    // If validation succeeded, go on to validate against database

    $username = $this->input->post('email');
    $password = $this->input->post('password');

    // query the database
    // $password = md5($password);

    $result = $this->user_data->loginValidation($username, $password);
    if ($result)
    {

      // add user data to the session

      $sess_array = array();
      foreach($result as $row)
      {
        $sess_array = array(
          'id' => $row->user_id,
          'email' => $row->email,
          'password' => $row->password,
          'logged_in' => TRUE,
          'state' => 'playQuestion'
          );

        // set the session

        $this->session->set_userdata($sess_array);
      }

      return TRUE;
    }
    else
    {
      $this->form_validation->set_message('check_database', "Sorry Invalid email or password");
      return false;
    }
  }

  function logout()
  {
    $this->session->unset_userdata('logged_in');

    // destroy the session

    $this->session->sess_destroy();
    redirect('login', 'refresh');
  }
  } //end of class

