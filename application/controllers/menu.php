<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends CI_Controller {


  public function __construct()
  {
    parent::__construct();
    
    $this->load->model('');            
  }


  public function index()
  {


   if (! $this->session->userdata('logged_in')==TRUE)
   {
    redirect('login');
  }


  $this->data['subview'] = 'menu';
  $this->load->view('layout_puzzle', $this->data);
}




}//end of class