<?php 
class QReviewResults extends CI_Model {

     function __construct()
    {
       
        parent::__construct();
    }
    

/**  
 * Following functions are for the 'progress' page and find the next review for each question
 */

function getUsersNextReview($user_id){

    $table_name = 'user_'.$user_id;
    $this->db->select('question_id');
    $this->db->select_min('nextReview');
    $this->db->where('user_id', $user_id);
    $this->db->order_by('nextReview', 'desc');
    $query = $this->db->get($table_name);
    return $query->row();//return as an object eg //$getQuestion->question;

}


function getUserNextReviewq1($user_id){

    $table_name = 'user_'.$user_id;
    $this->db->select('nextReview');
    $this->db->where('question_id', '1');
    $query = $this->db->get($table_name);
    return $query->row();//return as an object eg //$getQuestion->question;
}


function getUserNextReviewq2($user_id){

    $table_name = 'user_'.$user_id;
    $this->db->select('nextReview');
    $this->db->where('question_id', '2');
    $query = $this->db->get($table_name);
    return $query->row();
}

function getUserNextReviewq3($user_id){

    $table_name = 'user_'.$user_id;
    $this->db->select('nextReview');
    $this->db->where('question_id', '3');
    $query = $this->db->get($table_name);
    return $query->row();
}

function getUserNextReviewq4($user_id){

    $table_name = 'user_'.$user_id;
    $this->db->select('nextReview');
    $this->db->where('question_id', '4');
    $query = $this->db->get($table_name);
    return $query->row();
}

function getUserNextReviewq5($user_id){

    $table_name = 'user_'.$user_id;
    $this->db->select('nextReview');
    $this->db->where('question_id', '5');
    $query = $this->db->get($table_name);
    return $query->row();
}

function getUserNextReviewq6($user_id){

    $table_name = 'user_'.$user_id;
    $this->db->select('nextReview');
    $this->db->where('question_id', '6');
    $query = $this->db->get($table_name);
    return $query->row();
}

function getUserNextReviewq7($user_id){

    $table_name = 'user_'.$user_id;
    $this->db->select('nextReview');
    $this->db->where('question_id', '7');
    $query = $this->db->get($table_name);
    return $query->row();
}

function getUserNextReviewq8($user_id){

    $table_name = 'user_'.$user_id;
    $this->db->select('nextReview');
    $this->db->where('question_id', '8');
    $query = $this->db->get($table_name);
    return $query->row();
}

function getUserNextReviewq9($user_id){

    $table_name = 'user_'.$user_id;
    $this->db->select('nextReview');
    $this->db->where('question_id', '9');
    $query = $this->db->get($table_name);
    return $query->row();
}

function getUserNextReviewq10($user_id){

    $table_name = 'user_'.$user_id;
    $this->db->select('nextReview');
    $this->db->where('question_id', '10');
    $query = $this->db->get($table_name);
    return $query->row();
}



}//end of class