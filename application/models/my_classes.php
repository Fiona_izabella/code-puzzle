<?php 
class My_classes extends CI_Model {

	function __construct()
	{

		parent::__construct();
	}
	


/** 
 * get the question pieces from the dtatabase
 */
function getSnippets($question_number){
	$this->db->select('(select answer from codePiece where id = cp1) as answer1');
	$this->db->select('(select answer from codePiece where id = cp2) as answer2');
	$this->db->select('(select answer from codePiece where id = cp3) as answer3');
	$this->db->select('(select answer from codePiece where id = cp4) as answer4');
	$this->db->select('(select answer from codePiece where id = cp5) as answer5');

	$this->db->where('id', $question_number);
	$query = $this->db->get('question');
	if($query->num_rows() > 0)
	{

		return $query->result_array();
	}

}

/** 
 * get the question answers from the dtatabase
 */
function getSnippetAnswers($question_number){

	$this->db->select('(select answer from codePiece where id = answer1) as answer1');
	$this->db->select('(select answer from codePiece where id = answer2) as answer2');
	$this->db->select('(select answer from codePiece where id = answer3) as answer3');
	
	$this->db->where('id', $question_number);
	$query = $this->db->get('question');

	if($query->num_rows() > 0)
	{
		
		return $query->result_array();
	}

}


/** 
 * get the question ids from the database
 */
function getsnippetsids($question_number){


	$this->db->select('(select id from codePiece where id = cp1) as answer1');
	$this->db->select('(select id from codePiece where id = cp2) as answer2');
	$this->db->select('(select id from codePiece where id = cp3) as answer3');
	$this->db->select('(select id from codePiece where id = cp4) as answer4');
	$this->db->select('(select id from codePiece where id = cp5) as answer5');

	$this->db->where('id', $question_number);
	$query = $this->db->get('question');

	if($query->num_rows() > 0)
	{
		
		return $query->result_array();
	}
}

/** 
 * return the question count
 */
function getQuestionCount(){
	
	$user_id = $this->session->userdata('id');
	$table_name = 'user_'.$user_id;
	$this->db->select('question_id');
	$query = $this->db->get($table_name);
	return $query->result();

}

/** 
 * return the question 
 */
function getQuestion($question_number){

	$this->db->select('question');
	$this->db->where('id', $question_number);
	$query = $this->db->get('question');
		return $query->row();//return as an object eg //$getQuestion->question;

	}


/** 
 * return review date 
 */
function getReviewDate(){
	$user_id = $this->session->userdata('id');
	$table_name = 'user_'.$user_id;
	$this->db->select('nextReview');
	$query = $this->db->get($table_name);
	    return $query->result_array(); //return as an array eg //$getQuestion[0]['question'];


	}


/*
Get questions progress
 */

function getQuestionsProgress($user_id){

	$this->db->select('q, question_id, nextReview');
	$this->db->where('user_id', $user_id);
	$query = $this->db->get('review');
	return $query->result();

}

/** 
 * get the answers from the database for printing to page
 */
function getanswerForprint($question_number){

	$this->db->select('answer1, answer2, answer3');
	$this->db->where('id', $question_number);
	$query = $this->db->get('question');
	return $query->result_array();


}

/** 
 * get question info
 */
function getQuestionInfo($question_number){

	$user_id = $this->session->userdata('id');
	$table_name = 'user_'.$user_id;
	$this->db->select('n, newInt, EF');
	$this->db->where('question_id', $question_number);
	$query = $this->db->get($table_name);
	return $query->result_array();



}

/** 
 * get question number
 */

function getQuestionNumb(){
	$this->db->select('id');
	$query = $this->db->get('Question');
	return $query->result();

}



}//end of class