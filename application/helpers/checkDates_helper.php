<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**  
 *Helper function which checks whether the dates are within a range of 5 sdays
 */

function check_in_range($newDate, $newReviewDatePlus5, $today)
{
  // Convert to timestamp
  $start_ts = strtotime($newDate);
  $end_ts = strtotime($newReviewDatePlus5);
  $user_ts = strtotime($today);

  // Check that user date is between start & end
  return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
}