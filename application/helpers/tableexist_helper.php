<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**  
 *Helper function which checks whether a table exists to make sure a user does not create a duplicate table on registration
 */

function checkTableExists($question_numb){


	$CI =& get_instance();

	$user_id = $CI->session->userdata('id');
	$CI->db->select('n_value');
	$CI->db->where('user_id', $user_id);
	$CI->db->where('question_id',$question_numb);
	$query = $CI->db->get('review');

	return ( $query->num_rows() > 0 ) ? true : false;

}