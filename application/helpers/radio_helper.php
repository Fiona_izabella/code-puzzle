<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function checkRadioState(){

 /*set up helper function which detects whether the email notifications are set to true or false*/

	$CI =& get_instance();

	$user_id = $CI->session->userdata('id');
	$CI->db->select('notifications');
	$CI->db->where('user_id', $user_id);
	$query = $CI->db->get('users');
	 
	$answer = $query->row();

    //return true or false depending on value in database
    
	if ($answer->notifications == 1){
		return true;
	}  else{
		return false;
	}
 
}