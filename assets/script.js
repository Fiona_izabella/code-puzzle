/*Application javascript*/

/*toggle table functionality*/
$(".snippets-drag .showTable").click(function () {
//only open the table that is selected
$(this).closest("button").next('.results_table').toggle();
});


$('.close_table').click(function(){
  $('.results_table').hide();

});



$(function() {   
           $(".buttonS").click(function() { 
        
           // validate and process form here
           var radio_button_value;
          
           if ($("input[name='game-opt']:checked").length > 0){
               radio_button_value = $("input[name='game-opt']:checked").val();
           }
           else{
               alert("No button selected, try again!");
               return false;
           }

           var sendData = {
            value: radio_button_value 
            }
            
           $.ajax({
               type: "POST", 
               
               url: "baseurl(play/sessions')",  
                data: sendData,
               success: function() { 

                    console.log("form submitted: "+ radio_button_value);
                    console.log(sendData);
               }
            });
            return false;
         });
});

/*====== end of radio buttons======*/  


$('a.reload').click(function(){
  location.reload();
  alert("page is reloaded");
});


/*menu dialogue search box...*/
  $(function() {
var w = $(window).width();
    var h = $(window).height();

    $( "#dialog" ).dialog({
      autoOpen: false,
       height: h,
        width: w,
      show: {
        effect: "blind",
        duration: 1000
      },
      hide: {
        effect: "fold",
        duration: 1000
      },
       position: { 
        my: "center top", 
        at: "left top"

       },

    });
 
    $( "#opener" ).click(function() {
      $( "#dialog" ).dialog( "open" );
    });
  });


/*accordian*/

  (function($) {
    
  var allPanels = $('.accordion > dd').hide();
    
  $('.accordion > dt > a').click(function() {
    allPanels.slideUp();
    $(this).parent().next().slideDown();
    return false;
  });

})(jQuery);

/*live search for search dropdown*/

$(document).ready(function(){
    $("#filter").keyup(function(){
 
        // Retrieve the input field text and reset the count to '0'
        var filter = $(this).val(), count = 0;
 
        // Loop through the list of items
        $(".accordion dt a").each(function(){
      
 
            // If the list item does not contain the text phrase then fade it out
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();
 
            // Show the list item if the phrase matches and increase the count by '1'
            } else {
                $(this).show();
                count++;
            }
        });
 
        // Update the count
        var numberItems = count;
        $("#filter-count").text("Search result = "+count);
    });



    
    /*functionality for button which toggles the enabling and disabling of the play buttons in teh question menu*/
/*this should always be on, but it is set up for the project markets to turn off if they need to test the questions before the review date*/

(function($) {
    $.fn.toggleDisabled = function() {
        return this.each(function() {
            var $this = $(this);
            if ($this.attr('disabled')) $this.removeAttr('disabled').fadeTo('fast', 1);
            else $this.attr('disabled', 'disabled').fadeTo('fast', 0.5);
        });
    };
})(jQuery);

$(function() {
    $('button#disableQuestions').click(function() {
        $('input.notreadyButt').toggleDisabled();
         $('button#disableQuestions').toggleClass("btn-enabled");
         $(this).text($(this).text() == 'Question disabled : on' ? 'Question disabled : off' : 'Question disabled : on');
    });
});






});//end of document ready



